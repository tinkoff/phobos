libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % "2.6.19" % "provided",
  "com.typesafe.akka" %% "akka-http"   % "10.2.9",
)
